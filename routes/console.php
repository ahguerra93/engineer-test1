<?php

use Illuminate\Foundation\Inspiring;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function (App\ProcessMaker\ProcessMaker $pm) {
    $this->comment(Inspiring::quote().$pm->getAccessToken());
})->describe('Display an inspiring quote');

Artisan::command('processmaker:access', function (App\ProcessMaker\ProcessMaker $pm) {
    $this->comment($pm->getAccessToken());
})->describe('Get a ProcessMaker access_token');
