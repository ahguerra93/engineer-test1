<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Faker\Generator;
use App\ProcessMaker\ProcessMaker;

class UsersController extends Controller
{
	
	public function getaUsers()
	{
$PmInst=new ProcessMaker();
//$PmInst->connect();

$accessToken = isset($_COOKIE['146eeaf148dd84fb48803262f8564ba8ecd47f9f']) ? $_COOKIE['146eeaf148dd84fb48803262f8564ba8ecd47f9f'] : $PmInst->getAccessToken();
$apiServer = env('PROCESSMAKER_URL'); //set to your ProcessMaker address

$ch = curl_init($apiServer . "/api/1.0/workflow/users");
curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization: Bearer " . $accessToken));
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
$aUsers = json_decode(curl_exec($ch));
$statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
curl_close($ch);
$aActiveUsers = array();

if ($statusCode != 200) {
   if (isset ($aUsers) and isset($aUsers->error))
      print "Error code: {$aUsers->error->code}\nMessage: {$aUsers->error->message}\n";
   else
      print "Error: HTTP status code: $statusCode\n";
}
else {
   foreach ($aUsers as $oUser) {
      if ($oUser->usr_status == "ACTIVE") {
         $aActiveUsers[] = array("uid" => $oUser->usr_uid, "username" => $oUser->usr_username);
      }
   }
}
//dd($aUsers);
return $aUsers;
}
	public function index(Request $request, Generator $faker) {
		
		$allUsers = $this->getaUsers();
		$list = [];
		$list2 = [];
		foreach($allUsers as $aUser){
		$list2[]=[
		        "usr_username" => $aUser->usr_username,
		        "usr_email" => $aUser->usr_email,
		        "usr_create_date" => $aUser->usr_create_date,
		    ];
			}
		for($i=0; $i < 100; $i++) {
			$list[]=[
		        "usr_username" => $faker->name,
		        "usr_email" => $faker->unique()->safeEmail,
		        "usr_create_date" => $faker->date,
		    ];
	    }
	//	dd($list2);
	    return ["data" => $list2];
	//	return ["data" => $list];
	}

	public function approve() {
$PmInst=new ProcessMaker();
	
	$accessToken = isset($_COOKIE['146eeaf148dd84fb48803262f8564ba8ecd47f9f']) ? $_COOKIE['146eeaf148dd84fb48803262f8564ba8ecd47f9f'] : $PmInst->getAccessToken();
$apiServer = env('PROCESSMAKER_URL'); //set to your ProcessMaker address

$ch = curl_init($apiServer . "/api/1.0/workflow/plugin-test1/pass");
curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization: Bearer " . $accessToken));
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
$apUsers = json_decode(curl_exec($ch));
$statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
curl_close($ch);
//dd($apUsers);
$aActiveUsers = array();

if ($statusCode != 200) {
   if (isset ($apUsers) and isset($apUsers->error))
      print "Error code: {$apUsers->error->code}\nMessage: {$apUsers->error->message}\n";
   else
      print "Error: HTTP status code: $statusCode\n";
}
else {
   foreach ($apUsers as $oUser) {
      if ($oUser->usr_status == "ACTIVE") {
         $aActiveUsers[] = array("uid" => $oUser->usr_uid, "username" => $oUser->usr_username);
      }
   }
}
$list2 = [];
                foreach($apUsers as $aUser){
                $list2[]=[
                        "usr_username" => $aUser->usr_username,
                        "usr_email" => $aUser->usr_email,
                        "usr_create_date" => $aUser->usr_create_date,
                    ];
                      	}


return ["data" => $list2];

	}
}
